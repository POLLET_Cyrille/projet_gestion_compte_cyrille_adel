<?php

function creation_agence(&$agences,&$donnees3){
    echo("\n");
    echo (" ------------------------ CREER UNE AGENCE ------------------------ \n");
    $agence["nom"] = (strtolower(readline(" - Nom de l'agence : \n")));
    foreach($agences as $valeur){
        if($valeur["nom"] == $agence["nom"]){
            echo("\n");
            echo(" Agence déja existente !!! \n");
            return;
        }      
    }
    $agence["code"] = code_agence($donnees3);
    $agence["adresse"] = readline(" - Adresse de l'agence : \n");
    $agence["postal"] = readline(" - Code postal de l'agence : \n");
    
    $agences[] = $agence;
    echo ("\n                      !!!  Voici votre nouvelle agence !!! \n");
    echo("\n");
    echo("                               - Nom : ".$agence["nom"]."                   \n");
    echo("                               - Code : ".$agence["code"]."                  \n ");
    echo("                              - Adresse : ".$agence["adresse"]."               \n ");
    echo("                              - Postal : ".$agence["postal"]."                \n");
    echo("\n");
}

function creation_client(&$clients,&$donnees){
    echo("\n");
    echo (" ------------------------ CREER UN CLIENT ------------------------ \n");    
        $client["nom"] = strtoupper((readline(" - Nom client : \n")));
        $client["prenom"] = ucwords(strtolower(readline(" - Prenom client : \n")));
        $client["naissance"] = readline(" - Date de naissance (JJMMAAAA) : \n");
        $client["identifiant"] = strtoupper(code_aleatoire($donnees));
        $client["mail"] = readline(" - Mail du client : \n");
        foreach($clients as $valeur){
            if($valeur["mail"]==$client["mail"]){
                echo("\n");
                echo(" Le client existe déja !!! \n");
                return;
            }
        }
        $decouvert = strtolower(readline(" - Decouvert autorisé oui/non : \n"));
        while($decouvert != "oui" && $decouvert != "non"){
            $decouvert = strtolower(readline(" - Veuillez saisir oui ou non : ")); 
        }
        $client["decouvert"] = $decouvert;
        $clients[] = $client;
        echo ("\n                      !!!  Voici votre nouveau client !!! \n");
        echo("\n");
        echo("                               - Identifiant : ".$client["identifiant"]."           \n");
        echo("                                   - Nom : ".$client["nom"]."                   \n");
        echo("                                   - Prénom : ".$client["prenom"]."                  \n ");
        echo("                                  - Mail : ".$client["mail"]."                \n");
        echo("                                  - Découvert : ".$client["decouvert"]."                \n");
        echo("\n");
     
        
}  

function creation_compte(&$clients,$compte_a_ouvrir,&$comptes,&$donnees1,$agences,$id_possible2,&$donnees){
        identification($clients,$compte_a_ouvrir,$id_possible2,$donnees);
        if($compte_a_ouvrir=="a"){
            choix_a($comptes,$agences,$clients,$id_possible2,$donnees1);
        }
        if($compte_a_ouvrir == "b"){
            choix_b($comptes,$agences,$clients,$id_possible2,$donnees1);
        }
        if($compte_a_ouvrir == "c"){
            choix_c($comptes,$agences,$clients,$id_possible2,$donnees1);
        }
}

function recherche_compte($comptes){
    echo("-------------------  RECHERCHE COMPTE -----------------");
    echo("\n");
    $recherche=readline(" Veuillez saisir votre numéro de compte (11 chiffres) : ");
        $cpt=0;
        for ($i=0;$i<count($comptes);$i++){
            if ($recherche==$comptes[$i]["numero"]){
            echo (" Compte trouvé \n");
            echo "                     - Code agence : ".$comptes[$i]["codeagence"]."           \n";
            echo "                     - Identifiant: ".$comptes[$i]["identifiant"]."           \n";
            echo "                     - Nom : ".$comptes[$i]["nom"]."           \n";
            echo "                     - Prénom : ".$comptes[$i]["prenom"];
            echo "                     - Découvert : ".$comptes[$i]["decouvert"]."           \n";
            echo "                     - Solde : ".$comptes[$i]["solde"]."                  ";
            return;
            }
            $cpt++;
        }
        if($cpt == (count($comptes))){
            echo ("\n Compte non existant !!! ");
            return;
        }
}

function recherche_client($clients,$comptes){
    echo("-------------------  RECHERCHE CLIENT -----------------");
    echo("\n");
    $recherche=readline(" Veuillez saisir votre identifiant ou numéro de compte : ");
    echo("\n");
    $cpt=0;
    for ($i=0;$i<count($clients);$i++){
        if ($recherche==$clients[$i]["identifiant"]){
        echo (" Compte trouvé \n");
        echo "                     - Identifiant: ".$clients[$i]["identifiant"]."           \n";
        echo "                     - Nom : ".$clients[$i]["nom"]."           \n";
        echo "                     - Prénom : ".$clients[$i]["prenom"]."                    \n";
        echo "                     - Naissance : ".$clients[$i]["naissance"]."           \n";
        echo "                     - Mail : ".$clients[$i]["mail"]."                \n  ";
        return;
        }
        
    }
    for($i=0;$i<count($comptes);$i++){
       if($recherche == $comptes[$i]["numero"]){
        echo (" Compte trouvé \n");
        echo "                     - Identifiant: ".$clients[$i]["identifiant"]."           \n";
        echo "                     - Nom : ".$clients[$i]["nom"]."                           \n";
        echo "                     - Prénom : ".$clients[$i]["prenom"]."                   \n";
        echo "                     - Naissance : ".$clients[$i]["naissance"]."           \n";
        echo "                     - Mail : ".$clients[$i]["mail"]."                  \n";
        return;
       }

    }
    $cpt++;
    if($cpt == count($comptes)+1){
        echo ("\n Client non existant !!! ");
        return; 
    }
    
}

function recherche_compte_client($clients,$comptes){
    echo("-------------------  RECHERCHE COMPTE CLIENT -----------------");
    echo("\n");
    $cop=0;
    $recherche = strtoupper(readline(" Veuillez saisir l'identifiant du client : "));
    foreach($clients as $valeur){
        if($valeur["identifiant"]==$recherche){
          echo("\n");
          echo(" Infos client : \n");
          echo("                 - Identifiant : ".$valeur["identifiant"]."            \n");
          echo("                 - Nom : ".$valeur["nom"]."            \n");
          echo("                 - Prénom : ".$valeur["prenom"]."            \n");
          echo("                 - Naissance (JJMMAAA) : ".$valeur["naissance"]."            \n");
        }
        else {
           $cop++;            
        }
    }
    if($cop == count($clients)){
        echo("\n Client inexistant \n ");
    }
    echo("\n");
    echo(" Listes de(s) compte(s) : \n");
    $e=1;
    $ccp=0;
    foreach($comptes as $valeur){
        if ($valeur["identifiant"]== $recherche){
            echo(" - COMPTE N°".$e." : "."\n"."      - Numero : ".$valeur["numero"]."\n"."      - Solde : ".$valeur["solde"]."\n"."      - Decouvert : ".$valeur["decouvert"]."\n"."      - Type : ".$valeur["type"]."\n");
            $e++;
        }
        else{
            $ccp++;
        }
    }
    if ($ccp == count($comptes)){
        echo("\n Aucun compte \n");
        return;
    }
}

function imprimer_info($clients,$comptes){
    echo("\n");
    $recherche = strtoupper(readline(" - Veuillez saisir l'identifiant client : "));
    for($i=0;$i<count($clients);$i++){
        if($recherche == $clients[$i]["identifiant"]){
            $un=substr($clients[$i]["naissance"],0,2);
            $deux=substr($clients[$i]["naissance"],2,2);
            $trois=substr($clients[$i]["naissance"],4,4);
            file_put_contents('Fiche_client.txt',"------------------------------ FICHE CLIENT ------------------------------ \n "
            ."\n".
            " - Identifiant client : ".$clients[$i]["identifiant"]."\n".
            " - Nom : ".$clients[$i]["nom"]."\n".
            " - Prenom : ".$clients[$i]["prenom"]."\n".
            " - Date de naissance : ".$un."/".$deux."/".$trois."\n".
            "\n".
            "\n".
            "--------------------------------------------------------------------------"."\n".
            " Liste de compte "."\n".
            "--------------------------------------------------------------------------"."\n".
            " Numéro de compte                                      Solde : "."\n".
            "--------------------------------------------------------------------------"."\n");
            $fichier = file_get_contents('Fiche_client.txt');
            foreach($comptes as $valeur){
                if ($valeur["identifiant"]== $recherche){
                    $i=0;
                $fichier1 = " ".$valeur["numero"]."                                             ".$valeur["solde"]."\n";
                $fichier .= $fichier1;
                $i++;
                }
            }
            file_put_contents('Fiche_client.txt',$fichier);

            echo(" Votre fichier est pret a l'impression dans votre dossier : ");
            return;


        }
    }
    echo("\n Identifiant inconnu !!!  \n");
}
?>